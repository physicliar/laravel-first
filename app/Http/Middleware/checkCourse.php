<?php

namespace App\Http\Middleware;

use Closure;

class checkCourse
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->course<14){
            return redirect('SignUp');
        }
        return $next($request);
    }
}
