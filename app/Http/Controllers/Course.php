<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Course extends Controller
{
    public function index(){
        $arr['courses']  = \App\Course::all();
        return view('Course.course')->with($arr);
    }
}
