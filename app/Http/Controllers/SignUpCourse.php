<?php

namespace App\Http\Controllers;

use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SignUpCourse extends Controller
{
    public function index(Request $request){
        $request->validate([
            'name'=>'required|unique:courses',
            'info'=>'required|min:5',


        ]);
        try{
            DB::insert('insert into courses (name, info) values (?, ?)', [$request->input('name'), $request->input('info')]);
            echo "Course has successfully added!";
        }catch (QueryException $exception){
            print_r($exception);
        }

    }
}
