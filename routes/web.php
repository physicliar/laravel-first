<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;

Route::get('/', function () {
    return view('welcome');
});
Route::get('/SignUp', function (){
    return view( 'Course.Signup');
});

Route::get('/home', function()
{
    return View::make('pages.ev');
});

Route::get('/Course','Course@index')->middleware('course');
Route::post('/RequestSignUp', 'SignUpCourse@index');




