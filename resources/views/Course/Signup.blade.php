<h1> Login </h1>
@foreach($errors->all() as $error)
     <li>{{$error}}</li>
@endforeach
<form action="/RequestLogin" method="post">
    @csrf
    <div>
        <label>Course Name: </label>
        <input type="text" name="name">
    </div>
    <div>
        <label>Detail Information:</label>
        <input type="text" name="info">
    </div>

    <button type="submit">Login</button>
</form>