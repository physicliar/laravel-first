<!DOCTYPE html>
<html lang="en">

<link rel="stylesheet" href={{ asset('/css/app.css') }} >

    <section class="container">
        <div class="container-fluid">
            <table class="table table-bordered table-striped">
                <tr>
                    <th>ID</th>
                    <th>Course Name</th>
                    <th>Course Information</th>
                </tr>

                @foreach($courses as $course)
                 <tr>
                     <td>{!! $course->id !!}</td>
                     <td>{!! $course->name !!}</td>
                     <td>{!! $course->info !!}</td>
                 </tr>
                @endforeach
            </table>
        </div>
    </section>
</html>



