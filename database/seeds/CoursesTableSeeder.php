<?php

use Illuminate\Database\Seeder;

class CoursesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('courses')->insert([[
            "name" => "Course1",
            "info" => "Detail1"
        ],[
            "name" => "Course2",
            "info" => "Detail2"
        ],[
            "name" => "Course3",
            "info" => "Detail3"
        ],[
            "name" => "Course4",
            "info" => "Detail4"
        ]]);
    }
}
